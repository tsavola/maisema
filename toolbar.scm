;; Contants

(define toolbar-color "rgb:c0/c0/c0")


;; Button object

(define (make-button xo basename start-x start-y primary-action)

  (define end-x 0)
  (define end-y 0)

  (define name (string-append "pixmaps/" basename ".xpm"))
  (define name2 (string-append "pixmaps/" basename "2.xpm"))

  (define actions (list primary-action))

  (define visible #t)
  (define current-pixmap name)

  (define radiobutton #f)
  (define binding '())
  (define selected #f)

  (define (bind button)
    (set! radiobutton #t)
    (set! binding button)
    (set! selected #t)
    (set! act act-radiobutton)
    (binding 'unselect)
    (xo 'send 'CopyFromPixmap name2 start-x start-y)
    (set! current-pixmap name2))

  (define (unselect)
    (set! selected #f)
    (xo 'send 'SetWindowDrawing 'on 'on)
    (xo 'send 'CopyFromPixmap name start-x start-y)
    (xo 'send 'SetWindowDrawing 'off 'on)
    (set! current-pixmap name))

  (define (execute-actions)
    (define (loop l)
      (if (null? l)
	  'ok
	  (begin ((car l))
		 (loop (cdr l)))))
    (loop actions))

  (define (add-action new-action)
    (set! actions (cons new-action actions)))

  (define act
    (lambda (x y type)
      (let ((match (and (<= start-x x end-x)
			(<= start-y y end-y))))
	(if visible
	    (if type
		(if match
		    (begin (xo 'send 'SetWindowDrawing 'on 'on)
			   (xo 'send 'CopyFromPixmap name2 start-x start-y)
			   (xo 'send 'SetWindowDrawing 'off 'on)
			   (set! current-pixmap name2)
			   #t)
		    #f)
		(begin (xo 'send 'SetWindowDrawing 'on 'on)
		       (xo 'send 'CopyFromPixmap name start-x start-y)
		       (xo 'send 'SetWindowDrawing 'off 'on)
		       (set! current-pixmap name)
		       (if match
			   (execute-actions)
			   #f)))
	    #f))))
 
  (define act-radiobutton
    (lambda (x y type)
      (let ((match (and (<= start-x x end-x)
			(<= start-y y end-y))))
	(if visible
	    (if type
		(if match
		    (begin (xo 'send 'SetWindowDrawing 'on 'on)
			   (xo 'send 'CopyFromPixmap name2 start-x start-y)
			   (xo 'send 'SetWindowDrawing 'off 'on)
			   (set! current-pixmap name2)
			   #t)
		    #f)
		(if match
		    (begin (execute-actions)
			   (binding 'unselect)
			   #f)
		    (begin (xo 'send 'SetWindowDrawing 'on 'on)
			   (xo 'send 'CopyFromPixmap name start-x start-y)
			   (xo 'send 'SetWindowDrawing 'off 'on)
			   (set! current-pixmap name)
			   #f)))
	    #f))))

  (define (set-visible state)
    (set! visible state)
    (xo 'send 'SetWindowDrawing 'on 'on)
    (if state
	(xo 'send 'CopyFromPixmap current-pixmap start-x start-y)
	(begin (xo 'send 'SetForeground toolbar-color)
	       (xo 'send 'FillRectangle
		   start-x start-y
		   (- end-x start-x -1) (- end-y start-y -1))))
    (xo 'send 'SetWindowDrawing 'off 'on))

  (xo 'send 'ReadFileToPixmap name name)
  (xo 'send 'ReadFileToPixmap name2 name2)

  (let ((size (xo 'send-with-result 'GetPixmapSize name)))
    (set! end-x (+ start-x (cadr size) -1))
    (set! end-y (+ start-y (caddr size) -1)))

  (xo 'send 'CopyFromPixmap name start-x start-y)

  (lambda (signal . args) 
    (cond ((eq? signal 'act) (act (car args) (cadr args) (caddr args)))
	  ((eq? signal 'get-end-x) end-x)
	  ((eq? signal 'get-end-y) end-y)
	  ((eq? signal 'selected?) selected)
	  ((eq? signal 'unselect) (unselect))
	  ((eq? signal 'bind) (bind (car args)))
	  ((eq? signal 'set-visible) (set-visible (car args)))
	  ((eq? signal 'add-action) (add-action (car args)))
	  (else (error "unknown signal -- button" signal)))))


;; Toolbar object

(define (make-toolbar xo xo-width toolbar-height
		      new-action load-action save-action
		      3dview-action exit-action
		      bump-action hill-action
		      height-action radius-action steepness-action)

  (define width xo-width)
  (define height toolbar-height)

  (define pressed-button '())

  (define buttons
    (begin (xo 'send 'SetWindowDrawing 'on 'on)
	   (xo 'send 'SetForeground toolbar-color)
	   (xo 'send 'FillRectangle 0 0 width height)
	   (xo 'send 'SetWindowDrawing 'off 'on)
	   (let* ((new-b (make-button xo "new"
				      2 2
				      new-action))
		  (load-b (make-button xo "load"
				       (+ (new-b 'get-end-x) 3) 2
				       load-action))
		  (save-b (make-button xo "save"
				       (+ (load-b 'get-end-x) 3) 2
				       save-action))
		  (3dview-b (make-button xo "3dview"
					 (+ (save-b 'get-end-x) 11) 2
					 3dview-action))
		  (exit-b (make-button xo "exit"
				       (+ (3dview-b 'get-end-x) 11) 2
				       exit-action))
		  (bump-b (make-button xo "bump"
				       (+ (exit-b 'get-end-x) 31) 2
				       bump-action))
		  (hill-b (make-button xo "hill"
				       (+ (bump-b 'get-end-x) 3) 2
				       hill-action))
		  (height-b (make-button xo "height"
					 (+ (hill-b 'get-end-x) 11) 2
					 height-action))
		  (radius-b (make-button xo "radius"
					 (+ (height-b 'get-end-x) 3) 2
					 radius-action))
		  (steepness-b (make-button xo "steepness"
					    (+ (height-b 'get-end-x) 3) 2
					    steepness-action))
		  (bump-attribute-action (lambda ()
					   (steepness-b 'set-visible #f)
					   (radius-b 'set-visible #t)))
		  (hill-attribute-action (lambda ()
					   (radius-b 'set-visible #f)
					   (steepness-b 'set-visible #t))))
	     (hill-b 'bind bump-b)
	     (bump-b 'bind hill-b)
	     (hill-b 'add-action hill-attribute-action)
	     (bump-b 'add-action bump-attribute-action)
	     (bump-attribute-action)
	     (list new-b load-b save-b 3dview-b exit-b
		   bump-b hill-b height-b radius-b steepness-b))))

  (define (act x y type)
    (define (loop buttons)
      (if (null? buttons)
	  #f
	  (if ((car buttons) 'act x y #t)
	      (begin (set! pressed-button (car buttons))
		     #t)
	      (loop (cdr buttons)))))
    (if type
	(loop buttons)
	(if (null? pressed-button)
	    #f
	    (begin (pressed-button 'act x y #f)
		   (set! pressed-button '())))))

  (xo 'send 'RedrawWindowArea 0 0 width height)

  (lambda (signal . args)
    (cond ((eq? signal 'get-height) height)
	  ((eq? signal 'act) (act (car args)
				  (cadr args)
				  (caddr args)))
	  (else (error "unknown signal -- toolbar"
		       signal)))))
