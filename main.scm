;
; Dokumentaatio...
;


(load "terrain.scm")
(load "toolbar.scm")
(load "terraform.scm")
(load "render.scm")
(load "3d.scm")
(load "io.scm")


;; Constants

(define grid-width 512)
(define grid-height 512)

(define 3d-width 640)   ; widescreen ratio :)
(define 3d-height 360)  ;

(define toolbar-height 24)


;; Utility

(define (trunc x)
  (inexact->exact (floor x)))


;; Break Signal object

(define (make-break-signal)
  (define status #f)
  (lambda (signal)
    (cond ((eq? signal 'get) status)
	  ((eq? signal 'set) (set! status #t))
	  (else (error "unknown signal -- break-signal" signal)))))


;; Process Events procedure

(define (process-events xo button-handler break-signal)

  (define (button? event)
    (or (eq? (cadr event)
	     'ButtonPress)
	(eq? (cadr event)
	     'ButtonRelease)))

  (define (get-x event)
    (caddr event))

  (define (get-y event)
    (cadddr event))

  (define (get-type event)
    (eq? (cadr event)
	 'ButtonPress))

  (define (get-button event)
    (caddr (cdddr event)))

  (define pressed #f)

  (define (loop)
    (if (not (break-signal 'get))
	(let ((event (xo 'wait-for-event)))
	  (cond ((null? event)
		 (break-signal 'set))
		((button? event)
		 (set! pressed (get-type event))
		 (button-handler (get-x event)
				 (get-y event)
				 (get-type event)
				 (get-button event)))
		(else
		 'ignore))
	  (loop))
	'exit))

  (xo 'send 'ReportButtonEvents 'on 'on)
  (xo 'send 'ReportMotionEvents 'off)
  (xo 'send 'ReportKeyEvents 'off 'off)
  (loop))


;; Main procedure

(define (main)

  (define (transform-x x)
    (/ (* x (terrain 'get-width)) grid-width))

  (define (transform-y y)
    (/ (* (- y (toolbar 'get-height)) (terrain 'get-height)) grid-height))

  (define (handle-button x y type button)
    (if type
	(if (< y (toolbar 'get-height))
	    (toolbar 'act x y #t)
	    (begin (terraformer 'do-method
				(transform-x x) (transform-y y)
				(= button 1) terrain)
		   (grid-renderer 'update terrain)))
	(toolbar 'act x y #f)))

  (define new-help #t)
  (define (new-action)
    (if new-help
	(begin (display "Enter the horizontal and vertical dimensions of the grid; for example: 32 32")
	       (newline)
	       (set! new-help #f))
	'ignore)
    (display "Dimensions: ")
    (let ((x (read))
	  (y (read)))
      (if (and (<= 1 x grid-width) (<= 1 y grid-height))
	  (begin (set! terrain (make-terrain x y))
		 (grid-renderer 'clear 0)
		 (set! 3d-renderer '()))
	  (begin (display "*** Values must be less than the window size")
		 (newline)))))

  (define (load-action)
    (display "Load: ")
    (set! terrain (load-terrain (symbol->string (read))))
    (grid-renderer 'update terrain)
    (set! 3d-renderer '()))

  (define (save-action)
    (display "Save: ")
    (terrain 'set-name (symbol->string (read)))
    (save-terrain terrain))

  (define 3dview-help #t)
  (define (3dview-action)
    (define (n->s n) (number->string (inexact->exact (floor n))))
    (let ((sig (make-break-signal))
	  (pov-x 0) (pov-y 0)
	  (tgt-x 0) (tgt-y 0))
      (if 3dview-help
	  (begin (display "Select the point-of-view and the target by dragging the mouse.")
		 (newline))
	  'ignore)
      (process-events grid-xo
		      (lambda (x y type button)
			(if type
			    (begin (set! pov-x (transform-x x))
				   (set! pov-y (transform-y y)))
			    (begin (set! tgt-x (transform-x x))
				   (set! tgt-y (transform-y y))
				   (sig 'set))))
		      sig)
      (terrain 'set-highlight pov-x pov-y #t)
      (terrain 'set-highlight tgt-x tgt-y #t)
      (grid-renderer 'update terrain)
      (if (and (<= 0 pov-x) (< pov-x (terrain 'get-width))
	       (<= 0 pov-y) (< pov-y (terrain 'get-height)))
	  (let ((pitch (begin (display "Pitch angle: ")
			      (read))))
	    (if (and (number? pitch) (<= -90 pitch 90))
		(let ((xo (new-xdraw "-geometry"
				     (string-append (number->string 3d-width)
						    "x"
						    (number->string 3d-height))
				     "-title" "3D View"))
		      (sig (make-break-signal)))
		  (if (null? 3d-renderer)
		      (set! 3d-renderer
			    (make-3d-renderer terrain 3d-width 3d-height))
		      'ignore)
		  (3d-renderer 'render xo pov-x pov-y tgt-x tgt-y pitch)
		  (if 3dview-help
		      (begin (display "Click anywhere on the 3D View window to close it.")
			     (newline)
			     (set! 3dview-help #f))
		      'ignore)
		  (process-events xo (lambda (x y t b) (sig 'set)) sig)
		  (xo 'send 'Exit))
		(begin (display "*** Pitch angle must be in range [-90,90]")
		       (newline))))
	  (begin (display "*** Point-of-view out of bounds")
		 (newline)))
      (terrain 'set-highlight pov-x pov-y #f)
      (terrain 'set-highlight tgt-x tgt-y #f)
      (grid-renderer 'update terrain)))

  (define bump-help #t)
  (define (bump-action)
    (terraformer 'set-method 'bump)
    (if bump-help
	(begin (display "Create bumps with the left mouse button and holes with the right.")
	       (newline)
	       (display "Height: ")
	       (display (terraformer 'get-height))
	       (newline)
	       (display "Radius: ")
	       (display (terraformer 'get-radius))
	       (newline)
	       (set! bump-help #f))
	'ignore))

  (define hill-help #t)
  (define (hill-action)
    (terraformer 'set-method 'hill)
    (if hill-help
	(begin (display "Create hills with the left mouse button and valleys with the right.")
	       (newline)
	       (display "Steepness: ")
	       (display (terraformer 'get-threshold))
	       (newline)
	       (set! hill-help #f))
	'ignore))

  (define (height-action)
    (display "Height: ")
    (terraformer 'set-height (read)))

  (define (radius-action)
    (display "Radius: ")
    (terraformer 'set-radius (read)))

  (define (steepness-action)
    (display "Steepness: ")
    (terraformer 'set-threshold (read)))

  (define grid-xo
    (let ((xo (new-xdraw "-geometry"
			 (string-append (number->string grid-width)
					"x"
					(number->string (+ grid-height
							   toolbar-height)))
			 "-title" "Terrain Editor")))
      (xo 'send 'SetWindowDrawing 'off 'on)
      xo))

  (define toolbar (make-toolbar grid-xo grid-width toolbar-height
				new-action load-action save-action
				3dview-action (lambda () (exit-signal 'set))
				bump-action hill-action
				height-action radius-action steepness-action))

  (define terrain (make-terrain 32 32))

  (define grid-renderer (make-grid-renderer grid-xo
					    0 (toolbar 'get-height)
					    grid-width grid-height))

  (define 3d-renderer '())

  (define terraformer (make-terraformer))

  (define exit-signal (make-break-signal))

  (bump-action)
  (process-events grid-xo handle-button exit-signal)
  (grid-xo 'send 'Exit))
