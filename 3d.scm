;; Constants

(define sorting-enabled #t)  ; slow

(define height-factor (/ 1 16))
(define pov-height 3)

(define sky-color "rgb:9f/df/ff")


;; Vertex object

(define (make-vertex ox oy oz)  ; original coords

  ; transformed coords
  (define tx 0)
  (define ty 0)
  (define tz 0)

  ; projected coords
  (define px 0)
  (define py 0)

  (define color 0)

  ; project the transformed coordinates to the screen
  (define (project half-width half-height)
    (let* ((factor (/ half-width tz)))
      (set! px (+ (* tx factor) half-width))
      (set! py (+ (- (* ty factor)) half-height))))

  ; add an offset vector (vertex) and multiply with a rotation matrix
  (define (transform mov rot)
    (let ((x (+ ox (mov 'get-x)))
	  (y (+ oy (mov 'get-y)))
	  (z (+ oz (mov 'get-z))))
      (set! tx (+ (* x (rot 'get 0 0))
		  (* y (rot 'get 0 1))
		  (* z (rot 'get 0 2))))
      (set! ty (+ (* x (rot 'get 1 0))
		  (* y (rot 'get 1 1))
		  (* z (rot 'get 1 2))))
      (set! tz (+ (* x (rot 'get 2 0))
		  (* y (rot 'get 2 1))
		  (* z (rot 'get 2 2))))))

  (lambda (signal . args)
    (cond ((eq? signal 'transform) (transform (car args) (cadr args)))
	  ((eq? signal 'project) (project (car args) (cadr args)))
	  ((eq? signal 'get-x) ox)
	  ((eq? signal 'get-y) oy)
	  ((eq? signal 'get-z) oz)
	  ((eq? signal 'set-y) (set! oy (car args)))
	  ((eq? signal 'get-depth) tz)
	  ((eq? signal 'get-projected-x) px)
	  ((eq? signal 'get-projected-y) py)
	  ((eq? signal 'set-color) (set! color (car args)))
	  ((eq? signal 'get-color) color)
	  (else (error "unknown signal -- vertex" signal)))))


;; Triangle object

(define (make-triangle v1 v2 v3)

  (define gradient (get-gradient))

  (define depth 0)

  ; actually this is three times the depth, but the division would be useless
  (define (calc-depth)
    (set! depth (+ (v1 'get-depth)
		   (v2 'get-depth)
		   (v3 'get-depth))))

  (define (draw xo xo-width xo-height)
    (define (decide-color)
      (define (xor a b) (and (not (and a b)) (or a b)))
      (let ((neg1 (< (v1 'get-color) 0))
	    (neg2 (< (v2 'get-color) 0))
	    (neg3 (< (v3 'get-color) 0))
	    (aver (/ (+ (v1 'get-color) (v2 'get-color) (v3 'get-color)) 3)))
	(if (xor (or neg1 neg2 neg3) (and neg1 neg2 neg3))
	    (if (< aver 0)
		0
		aver)
	    aver)))
    (if (and (> (v1 'get-depth) 0)
	     (> (v2 'get-depth) 0)
	     (> (v3 'get-depth) 0))
	(begin (xo 'send 'SetForeground (gradient 'get-color (decide-color)))
	       (xo 'send 'FillPolygon
		   (list (list (trunc (v1 'get-projected-x))
			       (trunc (v1 'get-projected-y)))
			 (list (trunc (v2 'get-projected-x))
			       (trunc (v2 'get-projected-y)))
			 (list (trunc (v3 'get-projected-x))
			       (trunc (v3 'get-projected-y))))
		   'Convex 'Origin)
	       #t)
	#t))

  (lambda (signal . args)
    (cond ((eq? signal 'draw) (draw (car args) (cadr args) (caddr args)))
	  ((eq? signal 'calc-depth) (calc-depth))
	  ((eq? signal 'get-depth) depth)
	  (else (error "unknown signal -- triangle" signal)))))


;; 3x3 rotation Matrix object

(define (make-matrix around-x around-y)

  (define pi-2 (* (acos -1) 2))

  (define data
    ; create a rotation matrix based on pitch and heading angles
    (let* ((x-temp (if (< around-x 0) (+ around-x pi-2) around-x))
	   (y (if (< around-y 0) (+ around-y pi-2) around-y))
	   (x (* x-temp (cos y)))
	   (z 0);(* x-temp (sin y)))
	   (xs (sin x)) (ys (sin y)) (zs (sin z))
	   (xc (cos x)) (yc (cos y)) (zc (cos z)))
	(vector (vector (* yc zc)
			(* z yc)
			(- y))
		(vector (- (* x y zc) (* z xc))
			(+ (* x y z) (* xc zc))
			(* x yc))
		(vector (+ (* xc y zc) (* x z))
			(- (* xc y z) (* x zc))
			(* xc yc)))))

  (define (get col row)
    (vector-ref (vector-ref data row) col))

  (lambda (signal . args)
    (cond ((eq? signal 'get) (get (car args) (cadr args)))
	  (else (error "unknown signal -- matrix" signal)))))


;; 3D Renderer object

(define (make-3d-renderer terrain xo-width xo-height)

  (define grid-x (terrain 'get-width))
  (define grid-y (terrain 'get-height))

  (define vertex-count (* grid-x grid-y))
  (define vertices (make-vector vertex-count))

  (define triangle-count (* (- grid-x 1) (- grid-y 1) 2))
  (define triangles (make-vector triangle-count))

  (define (deg->rad a)
    (/ (* a (acos -1)) 180))

  (define (transform-vertices index mov rot)
    (if (< index vertex-count)
	(let ((vertex (vector-ref vertices index)))
	  (vertex 'transform mov rot)
	  (transform-vertices (+ index 1) mov rot))
	'ok))

  ; project vertices if their depth is greater than zero
  (define (project-vertices index half-width half-height)
    (if (< index vertex-count)
	(let ((vertex (vector-ref vertices index)))
	  (if (> (vertex 'get-depth) 0)
	      (vertex 'project half-width half-height)
	      'skip)
	  (project-vertices (+ index 1) half-width half-height))
	'ok))

  ; bubble sort - farthest (first drawn) triangles first
  (define (sort-triangles)
    (define (calc-depth index)
	(if (< index triangle-count)
	    (begin ((vector-ref triangles index) 'calc-depth)
		   (calc-depth (+ index 1)))
	    'ok))
    (define (outer-loop)
      (define (inner-loop index modified)
	(if (< index triangle-count)
	    (let* ((index-1 (- index 1))
		   (t1 (vector-ref triangles index-1))
		   (t2 (vector-ref triangles index)))
	      (if (< (t1 'get-depth) (t2 'get-depth))
		  (begin (vector-set! triangles index-1 t2)
			 (vector-set! triangles index t1)
			 (inner-loop (+ index 1) #t))
		  (inner-loop (+ index 1) modified)))
	    modified))
      (if (inner-loop 1 #f)
	  (outer-loop)
	  'ok))
    (calc-depth 0)
    (outer-loop))

  (define (render xo pov-x pov-y tgt-x tgt-y pitch)

    (define (calc-heading)
      (let ((x-delta (- tgt-x pov-x))
	    (y-delta (- tgt-y pov-y)))
	(if (= y-delta 0)
	    (cond ((= x-delta 0) 0)
		  ((> x-delta 0) (/ (acos -1) 2))
		  ((< x-delta 0) (/ (acos -1) -2)))
	    (let ((tan-of-angle (/ x-delta y-delta)))
	      (if (< y-delta 0)
		  (atan tan-of-angle)
		  (+ (atan tan-of-angle) (acos -1)))))))

    (define (calc-pov-height)
      (let ((height ((terrain 'get-block pov-x pov-y) 'get-height)))
	(if (> height 0)
	    (+ (* height height-factor) pov-height)
	    pov-height)))

    (define (setup-vertices i iter)
      (if (< i vertex-count)
	  (let ((height ((iter 'next) 'get-height))
		(vertex (vector-ref vertices i)))
	    (vertex 'set-y (if (> height 0)
			       (* height height-factor)
			       0))
	    (vertex 'set-color height)
	    (setup-vertices (+ i 1) iter))
	  'ok))

    (define (draw-triangles index)
      (if (< index triangle-count)
	  (if ((vector-ref triangles index) 'draw xo xo-width xo-height)
	      (draw-triangles (+ index 1))
	      'ok)
	  'ok))

    (setup-vertices 0 (terrain 'get-iterator))
    (transform-vertices 0 (make-vertex (- pov-x) (- (calc-pov-height)) pov-y)
			(make-matrix (deg->rad pitch) (calc-heading)))
    (project-vertices 0 (/ xo-width 2) (/ xo-height 2))
    (if sorting-enabled
	(begin (xo 'send 'DrawString 30 40 "Sorting triangles...")
	       (sort-triangles))
	'skip)
    (xo 'send 'SetForeground sky-color)
    (xo 'send 'FillRectangle 0 0 xo-width xo-height)
    (draw-triangles 0))

  ; iterates vertices by block - always represents the same corner
  (define (make-vertex-iterator corner)
    (let* ((index (cond ((eq? corner 'up-left) 0)
			((eq? corner 'up-right) 1)
			((eq? corner 'down-left) grid-x)
			((eq? corner 'down-right) (+ grid-x 1))
			(else
			 (error
			  "invalid corner -- 3d-renderer make-vertex-iterator"
			  corner))))
	   (column 0))
      (lambda (signal)
	(cond ((eq? signal 'has-next?)
	       (< index vertex-count))
	      ((eq? signal 'next)
	       (let ((vertex (vector-ref vertices index)))
		 (set! index (+ index 1))
		 (set! column (+ column 1))
		 (if (= column (- grid-x 1))
		     (begin (set! index (+ index 1))
			    (set! column 0))
		     'ignore)
		 vertex))
	      (else (error "unknown signal -- 3d-renderer vertex-iterator"
			   signal))))))

  (define (init-vertices index iterator)
    (if (< index vertex-count)
	(let ((coords ((iterator 'next) 'get-coords)))
	  (vector-set! vertices index (make-vertex (car coords)
						   0
						   (- (cdr coords))))
	  (init-vertices (+ index 1) iterator))
	'ok))

  (define (init-triangles index it1 it2 it3 it4)
    (if (it4 'has-next?)
	(let* ((v1 (it1 'next))
	       (v2 (it2 'next))
	       (v3 (it3 'next))
	       (v4 (it4 'next)))
	  (begin (vector-set! triangles index (make-triangle v1 v2 v3))
		 (vector-set! triangles (+ index 1) (make-triangle v2 v4 v3))
		 (init-triangles (+ index 2) it1 it2 it3 it4)))
	'ok))

  (init-vertices 0
                 (terrain 'get-iterator))

  (init-triangles 0
		  (make-vertex-iterator 'up-left)
		  (make-vertex-iterator 'up-right)
		  (make-vertex-iterator 'down-left)
		  (make-vertex-iterator 'down-right))

  (lambda (signal xo pov-x pov-y tgt-x tgt-y pitch)
    (if (eq? signal 'render)
	(render xo pov-x pov-y tgt-x tgt-y pitch)
	(error "unknown signal -- 3d-renderer" signal))))
