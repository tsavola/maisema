;; Load Terrain procedure

(define (load-terrain filename)
  (let* ((file (open-input-file (string-append "terrains/" filename)))
	 (terrain (make-terrain (read file)
				(read file))))
    (define (loop iterator)
      (if (iterator 'has-next?)
	  (begin ((iterator 'next) 'set-height (read file))
		 (loop iterator))
	  'ok))
    (loop (terrain 'get-iterator))
    (close-input-port file)
    terrain))


;; Save Terrain procedure

(define (save-terrain terrain)
  (define (loop iterator)
    (if (iterator 'has-next?)
	(begin (display ((iterator 'next) 'get-height))
	       (newline)
	       (loop iterator))
	'ok))
  (with-output-to-file (string-append "terrains/" (terrain 'get-name))
    (lambda ()
      (display (terrain 'get-width))
      (display " ")
      (display (terrain 'get-height))
      (newline)
      (newline)
      (loop (terrain 'get-iterator)))))
