;; Gradient singleton object  (Scheme is amazing... :-)

(define (get-gradient)

  (define (make-gradient)

    (define storage (make-vector 512 0))

    (define (rgb->x-color r g b)
      (define (nybble->char n)
	(integer->char (if (< n 10)
			   (+ 48 n)
			   (+ 87 n))))
      (define (integer->hex-string num)
	(let* ((upper (trunc (/ num 16)))
	       (lower (- (trunc num) (* upper 16))))
	  (string (nybble->char upper) (nybble->char lower))))
      (string-append "rgb:" (integer->hex-string r)
		     "/" (integer->hex-string g)
		     "/" (integer->hex-string b)))

    (define (calc-red i)
      (if (>= i 0)
	  (if (< i 128)
	      (+ 64 i)
	      (+ 192 (/ (- i 128) 4)))
	  0))

    (define (calc-green i)
      (if (>= i 0)
	  (if (< i 128)
	      (- 255 i)
	      i)
	  (- 127 (/ (- -1 i) 2))))

    (define (calc-blue i)
      (if (>= i 0)
	  (if (< i 128)
	      64
	      (+ 64 (/ (* (- i 128) 4) 3)))
	  (+ 256 i)))

    (define (get-color index highlighted)
      (define (clip x)
	(cond ((< x 0) 0)
	      ((>= x 256) 255)
	      (else (trunc x))))
      (if highlighted
	  (rgb->x-color (clip (+ (calc-red index) 64))
			(clip (+ (calc-green index) 64))
			(clip (+ (calc-blue index) 64)))
	  (let ((x-color (vector-ref storage (+ index 256))))
	    (if (string? x-color)
		x-color
		(let ((x-color (rgb->x-color (clip (calc-red index))
					     (clip (calc-green index))
					     (clip (calc-blue index)))))
		  (vector-set! storage (+ index 256) x-color)
		  x-color)))))

    (lambda (signal index . args)
      (if (eq? signal 'get-color)
	  (let ((highlighted (if (null? args) #f (car args))))
	    (cond ((< index -256) (get-color -256 highlighted))
		  ((>= index 256) (get-color 255 highlighted))
		  (else (get-color (trunc index) highlighted))))
	  (error "unknown signal -- gradient" signal))))

  (define instance (make-gradient))
  instance)


;; Grid Renderer object

(define (make-grid-renderer xo x-start y-start width height)

  (define gradient (get-gradient))

  (define (update terrain)

    (let* ((block-width (/ width (terrain 'get-width)))
	   (block-height (/ height (terrain 'get-height))))

      (define (draw-block block)
	(if (block 'is-modified?)
	    (begin (xo 'send 'SetForeground
		       (gradient 'get-color (block 'get-height)
				 (block 'is-highlighted?)))
		   (let ((coords (block 'get-coords)))
		     (xo 'send 'FillRectangle
			 (+ x-start (* (car coords) block-width))
			 (+ y-start (* (cdr coords) block-height))
			 block-width
			 block-height)))
	    'skip))

      (define (loop iterator)
	(if (iterator 'has-next?)
	    (begin (draw-block (iterator 'next))
		   (loop iterator))
	    'ok))

;      (xo 'send 'SetWindowDrawing 'on 'on)
      (loop (terrain 'get-iterator))
;      (xo 'send 'SetWindowDrawing 'off 'on)
      (xo 'send 'RedrawWindowArea x-start y-start width height)
      ))

  (define (clear index)
    (xo 'send 'SetForeground (gradient 'get-color index))
    (xo 'send 'FillRectangle x-start y-start width height)
    (xo 'send 'RedrawWindowArea x-start y-start width height))

  (clear 0)

  (lambda (signal arg)
    (cond ((eq? signal 'update) (update arg))
	  ((eq? signal 'clear) (clear arg))
	  (else (error "unknown signal -- grid-renderer" signal)))))
