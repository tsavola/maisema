;; Block object

(define (make-block x y . h)

  (define coords (cons x y))
  (define height (if (null? h) 0 (car h)))

  (define modified #t)
  (define highlighted #f)

  (define neighbors '())

  (define (set-height new-height)
    (if (eq? height new-height)
	'ignore
	(set! modified #t))
    (set! height new-height))

  (define (raise delta)
    (if (= delta 0)
	'ignore
	(set! modified #t))
    (set! height (+ height delta)))

  (define (is-modified?)
    (let ((temp modified))
      (set! modified #f)
      temp))

  (define (set-highlight state)
    (if (eq? state highlighted)
	'ignore
	(set! modified #t))
    (set! highlighted state))

  (define (calc-distance x y)
    (let ((x (- x (car coords)))
	  (y (- y (cdr coords))))
      (sqrt (+ (* x x) (* y y) 0.0))))

  (define (add-neighbor new)
    (if (null? new)
	'ignore
	(set! neighbors (cons new neighbors))))

  (lambda (signal . args)
    (cond ((eq? signal 'set-height) (set-height (car args)))
	  ((eq? signal 'get-height) height)
	  ((eq? signal 'get-coords) coords)
	  ((eq? signal 'raise) (raise (car args)))
	  ((eq? signal 'is-modified?) (is-modified?))
	  ((eq? signal 'calc-distance) (calc-distance (car args) (cadr args)))
	  ((eq? signal 'is-highlighted?) highlighted)
	  ((eq? signal 'set-highlight) (set-highlight (car args)))
	  ((eq? signal 'add-neighbor) (add-neighbor (car args)))
	  ((eq? signal 'get-neighbors) neighbors)
	  (else (error "unknown signal -- block" signal)))))
  


;; Terrain object

(define (make-terrain cols rows)

  (define name "unnamed")

  (define (create-blocks)
    (define (create-rows row row-vector)
      (define (create-cols col row col-vector)
	(if (< col cols)
	    (begin (vector-set! col-vector col
				(make-block col row))
		   (create-cols (+ col 1) row col-vector))
	    col-vector))
      (if (< row rows)
	  (begin (vector-set! row-vector row
			      (create-cols 0 row (make-vector cols)))
		 (create-rows (+ row 1) row-vector))
	  row-vector))
    (create-rows 0 (make-vector rows)))

  (define blocks (create-blocks))

  (define (get-block col row)
    (if (and (<= 0 col) (< col cols) (<= 0 row) (< row rows))
	(vector-ref (vector-ref blocks (trunc row)) (trunc col))
	'()))

  (define (set-highlight x y state)
    (let ((block (get-block x y)))
      (if (null? block)
	  'ignore
	  (block 'set-highlight state))))

  (define (make-iterator)
    (define col-i 0)
    (define row-i 0)
    (define (has-next?)
      (or (< col-i cols)
	  (< row-i (- rows 1))))
    (define (next)
      (if (has-next?)
	  (if (< col-i cols)
	      (let ((block (get-block col-i row-i)))
		(set! col-i (+ col-i 1))
		block)
	      (begin (set! col-i 1)
		     (set! row-i (+ row-i 1))
		     (get-block 0 row-i)))
	  (error "no more blocks -- terrain iterator")))
    (lambda (signal)
      (cond ((eq? signal 'next) (next))
	    ((eq? signal 'has-next?) (has-next?))
	    (else (error "unknown signal -- terrain iterator" signal)))))

  (define (set-neighbors iterator)
    (if (iterator 'has-next?)
	(let* ((block (iterator 'next))
	       (coords (block 'get-coords))
	       (x (car coords))
	       (y (cdr coords)))
	  (block 'add-neighbor (get-block x (- y 1)))
	  (block 'add-neighbor (get-block (+ x 1) y))
	  (block 'add-neighbor (get-block x (+ y 1)))
	  (block 'add-neighbor (get-block (- x 1) y))
	  (set-neighbors iterator))
	'ok))

  (set-neighbors (make-iterator))

  (lambda (signal . args)
    (cond ((eq? signal 'get-block) (get-block (car args) (cadr args)))
	  ((eq? signal 'get-iterator) (make-iterator))
	  ((eq? signal 'get-width) cols)
	  ((eq? signal 'get-height) rows)
	  ((eq? signal 'set-name) (set! name (car args)))
	  ((eq? signal 'get-name) name)
	  ((eq? signal 'set-highlight) (set-highlight (car args)
						      (cadr args)
						      (caddr args)))
	  (else (error "unknown signal -- terrain" signal)))))
